import java.util.Vector;

public class Hanoi {
	String[] x;
	int h;
	int n;

	public Hanoi(String[] a, int n, int h) {

		this.x = input(a);
		this.h = h;
		this.n = n;

	}

	/*
	 * static void get(){ Vector<Hanoi>x=new Vector<Hanoi>(); for(int
	 * i=0;i<3;i++){ Hanoi a=new Hanoi("p"+i,"q"+i,"r"+i,i*2); x.add(a); }
	 * for(int i=0;i<3;i++){ print(x.elementAt(i)); } }
	 */
	static int num(String m) {
		if (m.equals("P"))
			return 0;
		else if (m.equals("Q"))
			return 1;
		else
			return 2;
	}

	static String[] input(String[] x) {
		String[] z = new String[x.length];
		for (int i = 0; i < x.length; i++)
			z[i] = x[i];
		return z;
	}

	static Hanoi get_min(Vector<Hanoi> z) {
		int min = z.elementAt(0).h;
		int t = 0;
		for (int i = 0; i < z.size(); i++)
			if (z.elementAt(i).h < min) {
				t = i;
				min = z.elementAt(i).h;
			}
		Hanoi a = z.elementAt(t);
		z.removeElementAt(t);
		return a;
	}

	static Vector<Hanoi> build(Hanoi o) {
		Vector<Hanoi> z = new Vector<Hanoi>();
		Vector<Hanoi> z1 = new Vector<Hanoi>();
		int h = 33;
		String p = "";
		String q = "";
		String r = "";
		int l = 0;
		int l1 = 0;
		z.add(o);
		z1.add(o);
		while (z1.elementAt(l).h != 0) {

			String[] t = input(z1.elementAt(l).x);
			if (z1.elementAt(l).x[0].equals("P")) {
				p = "Q";
				z1.elementAt(l).x[0] = p;
				h = 28 - num(p) - 4 * num(z1.elementAt(l).x[1]) - 9
						* num(z1.elementAt(l).x[2]);
				l1++;
				h = h + l1;
				Hanoi a = new Hanoi(z1.elementAt(l).x,
						z1.elementAt(l).x.length, h);
				z.add(a);
				p = "R";
				z1.elementAt(l).x[0] = p;
				h = 28 - num(p) - 4 * num(z1.elementAt(l).x[1]) - 9
						* num(z1.elementAt(l).x[2]);
				h = h + l1;
				Hanoi b = new Hanoi(z1.elementAt(l).x,
						z1.elementAt(l).x.length, h);
				z.add(b);

			} else if (z1.elementAt(l).x[0].equals("Q")) {
				p = "P";
				z1.elementAt(l).x[0] = p;
				h = 28 - num(p) - 4 * num(z1.elementAt(l).x[1]) - 9
						* num(z1.elementAt(l).x[2]);
				l1++;
				h = h + l1;
				Hanoi a = new Hanoi(z1.elementAt(l).x,
						z1.elementAt(l).x.length, h);
				z.add(a);
				p = "R";
				z1.elementAt(l).x[0] = p;
				h = 28 - num(p) - 4 * num(z1.elementAt(l).x[1]) - 9
						* num(z1.elementAt(l).x[2]);
				h = h + l1;
				Hanoi b = new Hanoi(z1.elementAt(l).x,
						z1.elementAt(l).x.length, h);
				z.add(b);
			} else if (z1.elementAt(l).x[0].equals("R")) {
				p = "p";
				z1.elementAt(l).x[0] = p;
				h = 28 - num(p) - 4 * num(z1.elementAt(l).x[1]) - 9
						* num(z1.elementAt(l).x[2]);
				l1++;
				h = h + l1;
				Hanoi a = new Hanoi(z1.elementAt(l).x,
						z1.elementAt(l).x.length, h);
				z.add(a);
				p = "Q";
				z1.elementAt(l).x[0] = p;
				h = 28 - num(p) - 4 * num(z1.elementAt(l).x[1]) - 9
						* num(z1.elementAt(l).x[2]);
				h = h + l;
				Hanoi b = new Hanoi(z1.elementAt(l).x,
						z1.elementAt(l).x.length, h);
				z.add(b);
			}
			for (int i = 1; i < z1.elementAt(l).x.length; i++) {
				String[] t1 = input(t);
				p = get_next(t1[i], t1, i);
				if (!p.equals("null"))
					t1[i] = p;
				else
					t1[i] = t1[i];
				h = 28 - num(t1[0]) - 4 * num(t1[1]) - 9 * num(t1[2]);
				h = h + l1;
				Hanoi b1 = new Hanoi(t1, t1.length, h);
				z.add(b1);
			}
			Hanoi s = get_min(z);
			z1.add(s);
			l++;
		}
		return z1;
	}

	static String search(String[] a, String c) {
		Vector<String> z = new Vector<String>();
		String k = "";
		for (int i = 0; i < a.length; i++) {
			if (a[i].equals(c)) {
				k = "null";
				;
				break;
			} else
				z.add(a[i]);

		}
		String er = "";
		for (int i = 0; i < z.size(); i++) {
			er = z.elementAt(i);
			for (int j = i + 1; j < z.size(); j++) {
				if (z.elementAt(j).equals(er))
					z.removeElementAt(j);
			}

		}
		for (int i = 0; i < z.size(); i++) {
			er = z.elementAt(i);
			for (int j = i + 1; j < z.size(); j++) {
				if (z.elementAt(j).equals(er))
					z.removeElementAt(j);
			}

		}
		if (z.size() == 2)
			return "null";
		else if (c.equals("P") && z.elementAt(0).equals("Q"))
			return "R";
		else if (c.equals("Q") && z.elementAt(0).equals("R"))
			return "P";
		else if (c.equals("P") && z.elementAt(0).equals("R"))
			return "Q";
		else if (c.equals("Q") && z.elementAt(0).equals("p"))
			return "R";
		else if (c.equals("R") && z.elementAt(0).equals("Q"))
			return "p";
		else if (c.equals("R") && z.elementAt(0).equals("P"))
			return "Q";
		else
			return k;
	}

	static String get_next(String a, String[] x, int n) {
		String[] z = new String[n];
		for (int i = 0; i < n; i++)
			z[i] = "";
		for (int i = 0; i < n; i++) {
			z[i] = x[i];
		}
		return search(z, a);
	}

	static void print(Hanoi a) {
		System.out.print("( ");
		System.out.print(a.x[0] + " " + a.x[1] + " " + a.x[2] + " " + "h= "
				+ a.h);
		System.out.print(" )");
		System.out.println();
	}

	public static void main(String[] args) {
		String[] b = { "P", "P", "P" };
		int h = 28 - num(b[0]) - 4 * num(b[1]) - 9 * num(b[2]);
		Hanoi g = new Hanoi(b, b.length, h);
		print(g);
		build(g);
	}
}
