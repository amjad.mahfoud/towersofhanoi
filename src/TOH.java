import java.io.*;

public class TOH {
	public static int count = 0;

	public static void main(String[] args) throws IOException {
		BufferedReader obj = new BufferedReader(
				new InputStreamReader(System.in));
		int n;
		System.out.println("Enter no of pegs:");
		n = Integer.parseInt(obj.readLine());
		hanoi(n, 'A', 'C', 'B');
		count++;
		System.out.println("No of steps :" + count);
	}

	static void hanoi(int n, char from, char to, char aux) {
		if (n == 1) {
			System.out.println("Move Disk 1 From " + from + " To " + to);
		} else {
			hanoi(n - 1, from, aux, to);
			System.out
					.println("Move disk " + n + " From " + from + " To " + to);
			count++;
			hanoi(n - 1, aux, to, from);
			count++;
		}
	}
}

/*
 * Output Enter no of pegs: 3 Move Disk 1 From A To C Move disk 2 From A To B
 * Move Disk 1 From C To B Move disk 3 From A To C Move Disk 1 From B To A Move
 * disk 2 From B To C Move Disk 1 From A To C No of steps :7
 */
